/* $Id$ */

/*
 * Declarations for getopt_long.  These are from both OpenBSD and
 * Linux manual pages.  I believe the interface is not
 * copyrighted/copyrightable.  The header should be compatible with
 * either implementation.
 */

#ifndef _GETOPT_LONG_H_
#define _GETOPT_LONG_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

enum {
  no_argument = 0,
  required_argument = 1,
  optional_argument = 2,
};

struct option {
  const char *name;
  int has_arg;
  int *flag;
  int val;
};

int getopt_long (int argc, char *const *argv, const char *optstring,
		 const struct option *longopts, int *longindex);
int getopt_long_only (int argc, char *const *argv, const char *optstring,
		      const struct option *longopts, int *longindex);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* !_GETOPT_LONG_H_ */
