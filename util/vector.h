/* $Id$ */

/*
 *
 * Copyright (C) 1998 David Mazieres (dm@uun.org)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */

#ifndef _VECTOR_H_
#define _VECTOR_H_ 1

#define VECTOR(type)				\
struct {					\
  int v_nalloc;					\
  int v_size;					\
  type *v_vec;					\
}

#define VECTOR_INIT(vp)				\
do {						\
  (vp)->v_nalloc = (vp)->v_size = 0;		\
  (vp)->v_vec = NULL;				\
} while (0)

#define VECTOR_CLEAR(vp) free ((vp)->v_vec)

#define VECTOR_GROW(vp)							\
(void) ((vp)->v_size >= (vp)->v_nalloc					\
 && ((vp)->v_nalloc = ((vp)->v_nalloc ? (vp)->v_nalloc << 1 : 1),	\
     (vp)->v_vec = xrealloc ((vp)->v_vec,				\
			     (vp)->v_nalloc * sizeof (*(vp)->v_vec))))

#define VECTOR_PUSH(vp, elm)			\
do {						\
  VECTOR_GROW (vp);				\
  (vp)->v_vec[(vp)->v_size++] = (elm);		\
} while (0)

#define VECTOR_POP(vp)					\
     ((vp)->v_size ? &(vp)->v_vec[--(vp)->v_size] : NULL)

#define VECTOR_NEXT(vp)				\
     (VECTOR_GROW(vp), &(vp)->v_vec[(vp)->v_size++])

#define VECTOR_LAST(vp)					\
     ((vp)->v_size ? &(vp)->v_vec[(vp)->v_size-1] : NULL)

#define VECTOR_RESERVE(vp, n)						\
do {									\
  if ((vp)->v_size + n > (vp)->v_nalloc) {				\
    do {								\
      && ((vp)->v_nalloc = (vp)->v_nalloc ? (vp)->v_nalloc << 1 : 1;	\
    } while ((vp)->v_size + n > (vp)->v_nalloc);			\
    (vp)->v_vec = xrealloc ((vp)->v_vec,				\
			    (vp)->v_nalloc * sizeof (*(vp)->v_vec));	\
  }									\
} while (0)

#endif /* !_VECTOR_H_ */
