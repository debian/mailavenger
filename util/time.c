/* $Id$ */

/*
 *
 * Copyright (C) 2003 David Mazieres (dm@uun.org)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */

#include "avutil.h"

long
parse_expire (const char *age, time_t now)
{
  unsigned long res = -1;
  char mult;
  unsigned int len;
  int sign = 1;

  if (age[0] == '+')
    age++;
  else if (age[0] == '-') {
    sign = -1;
    age++;
  }
  else if (sscanf (age, "%lu%n", &res, &len) == 1 && len == strlen (age))
    return res;

  if (sscanf (age, "%lu%c%n", &res, &mult, &len) != 2
      || len != strlen (age)) {
    fprintf (stderr, "bad time %s\n", age);
    return -1;
  }

  if (sign < 0)
    res = -res;
  switch (mult) {
  case 'Y':
    res *= 365 * 24 * 60 * 60;
    break;
  case 'M':
    res *= 30 * 24 * 60 * 60;
    break;
  case 'W':
  case 'w':
    res *= 7 * 24 * 60 * 60;
    break;
  case 'D':
  case 'd':
    res *= 24 * 60 * 60;
    break;
  case 'h':
    res *= 60 * 60;
    break;
  case 'm':
    res *= 60;
    break;
  case 's':
    break;
  default:
    fprintf (stderr, "unknown age unit '%c'\n", mult);
    return -1;
  }

  return now + res;
}

