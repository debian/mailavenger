/* $Id$ */

/*
 *
 * Copyright (C) 2004 David Mazieres (dm@uun.org)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */

#include "avutil.h"

/* Note:  Unfortunately this doesn't deal with lines containing '\0'
 * characters. */
int
readln (struct lnbuf *res, FILE *fp, size_t maxbuf)
{
  if (!res->alloc) {
    res->alloc = 128;
    res->buf = xmalloc (res->alloc);
  }
  if (!fgets (res->buf, res->alloc, fp)) {
    res->size = 0;
    return ferror (fp) ? LNBUF_IOERR : LNBUF_EOF;
  }

  for (;;) {
    size_t newalloc;

    res->size = strlen (res->buf);
    if (res->size && res->buf[res->size - 1] == '\n')
      return LNBUF_OK;
    else if (res->size < res->alloc - 1)
      return LNBUF_EOFNL;

    newalloc = res->alloc * 2;
    if (newalloc > maxbuf)
      newalloc = maxbuf;
    if (newalloc < res->alloc)
      newalloc = res->alloc;
    if (newalloc <= res->alloc)
      return LNBUF_TOOBIG;

    res->buf = xrealloc (res->buf, newalloc);
    res->alloc = newalloc;

    if (!fgets (res->buf + res->size, res->alloc - res->size, fp))
      return ferror (fp) ? LNBUF_IOERR : LNBUF_EOFNL;
  }
}
