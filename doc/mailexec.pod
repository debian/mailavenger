=for comment $Id$

=head1 NAME

mailexec - run program on messages in mbox file or maildir

=head1 SYNOPSIS

mailexec [-nvFR] I<mailbox> I<program> [I<arg> ...]

=head1 DESCRIPTION

mailexec runs a program over every message in I<mailbox>, which must
be an mbox-format mail file or maildir directory.  For each message
encountered, mailexec executes I<program> with the specified
arguments, supplying the message as standard input.  mailexec
synthesizes a C<S<From >> line and a C<Return-Path:> header field for
each message if the message does not already contain one.  (This
behavior can be modified by the B<-F> and B<-R> flags, described
below.)

When parsing mbox format files, mailexec unescapes C<From > lines.  If
a line begins with one or more C<E<gt>> characters followed by C<From
>, mailexec deletes one of the C<E<gt>> characters.  If you do not
want this unescaping behavior, see the L<formail(1)|formail(1)>
utility, which has a B<-s> flag that performs a similar function to
mailexec.

=head2 OPTIONS

=over

=item B<-n>

When processing a maildir, causes mailexec to look exclusively at
"new" messages that have not yet been moved to the F<cur> directory.

=item B<-v>

Turns on verbose mode.  If I<mailbox> is a maildir, mailexec prints
out the name of each file it processes inside the maildir.  If
I<mailbox> is an mbox format file, mailexec prints the message-id of
each message it processes (as long as messages have a message-id
header).

=item B<-F>

Suppresses printing of the initial C<From > line at the beginning of
each message.

=item B<-R>

Suppresses printing of initial C<Return-Path:> lines.

=back

=head1 EXAMPLES

To get the same behavior as the Unix L<from(1)|from(1)> command on a
mail directory F<dir>, you can run either of the following two
commands:

	mailexec -n dir head -1
	mailexec -n dir sed -ne 1p

To convert an mbox-format file F<mbox> into a maildir directory
F<dir>, you can run:

	mailexec mbox deliver dir/

Conversely, to convert maildir F<dir> into an mbox-format file
F<mbox>, run:

	mailexec dir deliver mbox

To train the spamassassin filter on a mail folder called I<spam>
containing unwanted messages, run:

	mailexec spam sa-learn --spam

Note that this works whether I<spam> is an mbox format file or a
maildir directory.

If you have an old mbox file or maildir directory F<box> and wish to
"import" the old mail into your web mail account, say
I<example@gmail.com>, you can run:

	mailexec -F box sendmail example@gmail.com

Note again that this works whether F<box> is an mbox format file or a
maildir directory.

=head1 SEE ALSO

L<avenger(1)|avenger(1)>,
L<deliver(1)|deliver(1)>,
L<dotlock(1)|dotlock(1)>,
L<avenger.local(8)|avenger.local(8)>

The Mail Avenger home page: L<http://www.mailavenger.org/>.

=head1 BUGS

When reading from a maildir and synthesizing a C<From > line, mailexec
guesses the delivery date of the message based on the name of the
file, which works in practice but could be considered dangerous since
file names in maildirs are supposed to be opaque.  Thus, while
generally mailexec places sensible dates in C<From > lines and
processes maildir messages in order of delivery, it might be unwise to
rely on this behavior.

mailexec generates the time for the C<From > line in the local time
zone, as is customary on Unix.  This could lead to loss of information
when transferring mailboxes across time zones or combining mailboxes
created in different timezones.  Moreover, this practice is
incompatible with qmail, which uses GMT in the C<From > line.

mailexec expects that if there is a C<Return-Path:> header field, it
will be the first header field in the message (possibly after the
initial C<From > line, which is not itself a header field).

There are many different variants of the mbox message format.
mailexec expects the "mboxrd" variant, in which each message is
delimited by a C<From > line at the beginning and a blank line at the
end, and every line beginning with either C<From > or one or more
C<E<gt>> characters followed by C<From > is escaped by adding another
C<E<gt>> character.  In particular, this means mailexec will
incorrectly parse System V "mboxcl2" files, which use
C<Content-Length:> header fields to determine message boundaries
rather than C<From > lines.

mailexec attempts to lock mbox format files, but will execute anyway
even if it cannot obtain the lock.  This allows it to work on
read-only files, but if you are highly unlucky could result in the
last message being truncated.

There is no locking for maildir files.  If a maildir is modified while
mailexec is running over it, mailexec could miss messages.  If you are
concurrently manipulating the maildir with a mail reader, maildir
could even miss old messages that just happen to have been moved from
the F<new> to the F<cur> directory.  mailexec will issue a warning if
it fails to open a file that it had previously seen when scanning the
directory.

=head1 AUTHOR

David MaziE<egrave>res
